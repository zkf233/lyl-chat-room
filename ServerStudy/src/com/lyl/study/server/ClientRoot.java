package com.lyl.study.server;

import com.lyl.study.server.util.ReadData;
import com.lyl.study.server.util.SendData;

import java.io.IOException;
import java.net.Socket;

/**
 * ClientRoot：client的root权限（管理员）
 * 方法：
 *  help：
 *      查看cmd详细
 *  remove -ClientName：
 *      将ClientName移除ClientList
 *  ls：
 *      查看当前Server中的ClientList
 */
public class ClientRoot {
    private final Server server;

    public ClientRoot(Server server) {
        this.server = server;

    }

    /*
     初始化Root
     相当于创建一个客户端
     */
    public void initRoot(String pwd){
        try {
            Socket socket = new Socket("localhost",server.getPort());
            new Thread(new SendData(socket,"root",pwd)).start();
            new Thread(new ReadData(socket)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
