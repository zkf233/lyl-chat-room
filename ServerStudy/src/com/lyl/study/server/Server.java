package com.lyl.study.server;

import com.lyl.study.server.util.LYLTool;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 服务端：只需要实现开启服务器，打开端口供Client连接，同一个端口号的服务器不要多开
 * 注意：端口号最好是在7000以上，以免端口号冲突
 */
public class Server implements Runnable {
    private final CopyOnWriteArrayList<ClientContent> clientList;
    private boolean isRunning,isRunRoot;
    private final int port;
    private String pwd;
    private ServerSocket server;

    /**
     *  得到客户端容器 （管理员权限）
     * @return 返回客户端容器
     */
    public CopyOnWriteArrayList<ClientContent> getClientList() {
        return clientList;
    }

    /**
     * 初始化服务器
     * @param port 端口号
     */
    Server(int port,boolean isRunRoot,String pwd){
        this.isRunning = true;
        this.port = port;
        this.isRunRoot = isRunRoot;
        this.pwd = pwd;
        this.clientList = new CopyOnWriteArrayList<>();
        System.out.println("初始化服务器成功！服务器端口号为："+port+"密码为："+pwd);
    }
    private void initRoot(){
        if(isRunRoot){
            // 允许开启Root 初始化
            ClientRoot root = new ClientRoot(this);
            root.initRoot(this.pwd);
            System.out.println("root初始化成功");
        }
    }

    public int getPort() {
        return port;
    }

    public static void main(String[] args) {
        new Thread(new Server(9999,true,"123")).start();
    }

    /**
     * 初始化服务器
     */
    private void initServer(){
        try {
            server = new ServerSocket(port);
            if(isRunRoot){
                initRoot();
            }
            while(isRunning){
                initContent();
            }
        } catch (IOException e) {
            isRunning = false;
            LYLTool.closeAllIO(server);
            e.printStackTrace();
        }
    }

    /**
     * 移除客户机
     * @param client 传来的Client实例
     */
    public String removeClient(ClientContent client){
        if(client!=null){
            clientList.remove(client);
            client.close();
            return "移除"+client.getName();
        }
        return "";
    }

    private void initContent(){
        try {
            Socket socket = server.accept();
            // 判断客户端密码是否符合服务器规定的密码
            ClientContent client = new ClientContent(socket,this,this.pwd);

            if(isRunRoot) {
                client.setRoot(true);
                isRunRoot = false;
            }
            // 检查是否客户端是否创建成功
            if (client.isTrue()) {
                clientList.add(client);
                new Thread(client).start();
                System.out.println(clientList.size() + "-------->  " + this.port);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        initServer();
    }
}
