package com.lyl.study.server;

import com.lyl.study.server.util.ReadData;
import com.lyl.study.server.util.SendData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * 客户端：以为要使用OOP思想封装，因此客户端只需要实现连接Server，打开ReadData和SendData即可，并不需要做其他操作
 */
public class Client {
    // 提供客户端接口供外部使用
    private Socket client;
    private String ipPath;
    private int port;
    private String pwd;

    public static void main(String[] args) {
        Client client = new Client("localhost", 9999,"123");
        client.openConnect();
        new Thread(()->{


        });

    }

    /**
     * 客户端直连
     *
     * @param ipPath 服务器IP地址
     * @param port   服务器开发的端口号
     */

    Client(String ipPath, int port,String pwd) {
        this.ipPath = ipPath;
        this.port = port;
        this.pwd = pwd;

    }

    /**
     * 客户端直连
     *
     * @param client 客户端实例
     */
    Client(Socket client) {
        this.client = client;
    }

    /**
     * 正则匹配用户名是否合法
     *
     * @param name 控制台输入的name
     * @return 返回匹配的boolean
     */
    private boolean checkName(String name) {
        String rex = "^[-_a-zA-Z0-9\\u4e00-\\u9fa5]{4,16}$";
        return name.matches(rex);
    }

    /**
     * 打开连接
     */
    public void openConnect() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));// 由控制台得到用户输入流
            System.out.println("请输入用户名（4-16位）：");
            String name = br.readLine();
            while (!checkName(name)) {
                System.out.println("用户名不合法，请输入合法用户名：");
                name = br.readLine();
            }
            if (client == null) {
                this.client = new Socket(this.ipPath, this.port);

            }
            new Thread(new ReadData(client)).start();
            new Thread(new SendData(client, name,pwd)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
