package com.lyl.study.server.util;

import com.lyl.study.server.ClientContent;
import com.lyl.study.server.Server;

import java.io.Closeable;
import java.text.SimpleDateFormat;
import java.util.Date;


/*
工具类
 */
public class LYLTool {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 时间格式化

    /**
     * 关闭工具
     *
     * @param taps 流的集合
     */
    public static void closeAllIO(Closeable... taps) {
        try {
            for (Closeable tap : taps) {
                if (tap != null)
                    tap.close();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 显示当前系统时间，以服务器的系统时间为标准
     */
    public static String showData() {
        return SDF.format(new Date());// 获取当前时间并格式化
    }

    /**
     * 判断client用户名有没有重复
     *
     * @param clientName client用户名
     * @return 根据返回值判断传入的用户名有无重复，目前用户名是方便其他client之间绑定唯一标识，所以不支持用户名重复
     */
    public static boolean clientNameIsRepeat(String clientName, Server server) {
        for (ClientContent client : server.getClientList()) {
            if (client.getName().equals(clientName)) {
                return true;
            }
        }
        return false;
    }
}
