package com.lyl.study.server.util;

import java.io.*;
import java.net.Socket;

/**
 * 发送端：实现Runnable接口，开辟线程处理数据读取（避免读取和发送不能分开）
 */
public class SendData implements Runnable {
    private Socket socket;
    private BufferedReader br;
    private DataOutputStream dos;
    private boolean isRunning;

    /**
     * 初始化发送端
     * @param socket 客户端实例
     * @param name 用户名标识
     */
    public SendData(Socket socket, String name,String pwd){
        try {
            this.socket = socket;
            this.isRunning = true;
            br = new BufferedReader(new InputStreamReader(System.in));// 获取控制台的输入流
            dos = new DataOutputStream(socket.getOutputStream());
            send(pwd);// 将pwd发送给服务器
            send(name);// 将name发送给服务器
        } catch (IOException e) {
            System.out.println("----1----");
            this.close();
        }
    }

    /**
     * 接受来自控制台的输入流
     * @return 返回输入流的数据
     */
    private String getScanner(){
        try {
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            this.close();
        }
        return "";
    }

    /**
     * 发送消息
     * @param msg 消息内容
     */
    private void send(String msg){
        try {
            dos.writeUTF(msg);
            dos.flush();
        } catch (IOException e) {
            System.out.println("----3----");
            this.close();
        }
    }

    @Override
    public void run() {
        while(isRunning){
            String msg = getScanner();
            if(!msg.equals("")){
                send(msg);
            }
        }
    }

    /**
     * 关闭
     */
    private void close(){
        isRunning = false;
        LYLTool.closeAllIO(br,dos,socket);
    }
}
