package com.lyl.study.server.util;

import com.lyl.study.server.ClientContent;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

/**
 * root命令的处理
 * 命令格式：  ":root type content" 注意：冒号目前只支持英文冒号
 * 用于接收传来的cmd，来处理指定的命令
 */
public class LYLCmd {
    /**
     * remove 移除Client
     *
     * @param clientList 当前Server下的所有Client
     * @param cmd        cmd命令组
     * @return 根据找到的ClientName返回对应的ClientContent交给Server处理，没有则返回null
     */
    public static ClientContent remove(List<ClientContent> clientList, String[] cmd) {
        if (cmd.length < 3) {
            System.out.println("请输入正确的用户名");
            return null;
        }
        String clientName = cmd[2];
        for (ClientContent client : clientList) {
            if (client.getName().equals(clientName)) {
                System.out.println("已经成功将" + clientName + "移除");
                return client;
            }
        }
        System.out.println("未找到客户机");
        return null;
    }

    /**
     * 查看Server下所有的client name
     *
     * @param clientList Server下的所有Client
     * @return 以String数据类型，返回Server下的所有ClientName
     */
    public static String lsClient(List<ClientContent> clientList) {
        StringBuilder sb = new StringBuilder();
        sb.append("该服务器共有以下Client连接：");
        for (ClientContent client : clientList) {
            sb.append(client.getName()).append(" ");
        }
        sb.append("。");
        return sb.toString();
    }

    /**
     * 查看命令格式和帮助
     * 读取文件地址的方式是类加载器，由于是动态的，所以查找的是经过编译后的类地址
     *
     * @return 将cmd_help文件的内容返回
     */
    public static String help() {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader file = new BufferedReader(new FileReader(LYLCmd.class.getResource("cmd_help.txt").getPath()));
            String len;
            while ((len = file.readLine()) != null) {
                sb.append(len).append("\r\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
