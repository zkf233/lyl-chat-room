package com.lyl.study.server.util;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * 读取数据：实现Runnable接口，开辟线程处理数据读取（避免读取和发送不能分开）
 */
public class ReadData implements Runnable {
    private final Socket socket;
    private DataInputStream dis;
    private boolean isRunning;

    /**
     * 接受端初始化
     *
     * @param socket 客户端实例
     */
    public ReadData(Socket socket) {
        this.socket = socket;
        this.isRunning = true;
        try {
            dis = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            System.out.println("----1----");
            this.close();
        }
    }

    /**
     * 读取
     *
     * @return 返回客户端读取流中的信息
     */
    private String receive() {
        try {
            return dis.readUTF();
        } catch (IOException e) {
            System.out.println("------2------");
            this.close();
        }
        return "";
    }

    @Override
    public void run() {
        while (isRunning) {
            String msg = receive();
            if (!msg.equals("")) {
                System.out.println(msg + "\r\n");
            }
        }
    }

    /**
     * 关闭
     */
    private void close() {
        isRunning = false;
        LYLTool.closeAllIO(dis, socket);
    }
}
