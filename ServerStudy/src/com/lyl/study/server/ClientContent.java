package com.lyl.study.server;

import com.lyl.study.server.util.LYLCmd;
import com.lyl.study.server.util.LYLTool;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

/**
 * 核心代码：客户端真正的实例（服务端获取的客户端实例）
 * 以为Server要处理多个Client连接，为了实现私聊和群聊的效果，Server需要获取到Client的实例，所以将Client的主题业务代码封装在ClientContent中，让Server获取ClientContent的实例即可
 * <p>
 * 新增一个变量和cmd处理的方法
 */
public class ClientContent implements Runnable {
    private final Socket socket;
    private DataOutputStream dos;
    private DataInputStream dis;
    private String name;
    private String pwd;
    private boolean isRunning, isRoot;
    private final Server server;

    /**
     * 处理cmd
     *
     * @param msg 得到控制台输入的cmd命令
     */
    private void processingCMD(String msg) {
        // 将msg拆分
        String[] cmd = msg.split(" ");
        switch (cmd[1]) {
            case "remove":
                System.out.println(server.removeClient(LYLCmd.remove(server.getClientList(), cmd)));
                break;
            case "ls":
                System.out.println(LYLCmd.lsClient(server.getClientList()));
                break;
            case "help":
                System.out.println(LYLCmd.help());
                break;
            default:
                System.out.println("该工具不支持此命令。请尝试用\"help\"");
        }
    }

    @Override
    public void run() {
        while (isRunning) {
            String msg = receive();
            if (isRoot && msg.split(" ")[0].equals(":root")) {
                processingCMD(msg);
            } else {
                sendAllClient(msg, false);
            }
        }
    }


    /**
     * @param socket 提供客户端实例
     * @param server 客户端连接的服务器实例，服务器的唯一性保障了客户机获取不到管理员的权限
     */
    ClientContent(Socket socket, Server server,String pwd) {
        this.socket = socket;
        this.server = server;
        this.pwd = pwd;
        this.isRunning = true;
        initClient();
    }

    public void setRoot(boolean root) {
        isRoot = root;
    }

    /**
     * 初始化客户端
     */
    private void initClient() {
        //初始化流
        try {
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
            String password = receive();// 得到客户机传来的密码
            System.out.println(password);
            if(!password.equals(this.pwd)){
                System.out.println("密码错误");
                send("密码错误！");
                return;
            }
            this.name = receive();// 得到客户机传来的用户标识名
            System.out.println("服务器接收的值为："+name);
            // 由客户端发来的合法用户名，在服务器中查重，服务器再返回给客户端一个值
            while (LYLTool.clientNameIsRepeat(name, server)) {
                send("用户名不合法");
                this.name = receive();
            }
            System.out.println(this.name);
            send("欢迎加入聊天室");// 返回给客户机
            sendAllClient(this.name + "加入聊天室。。。", false);

        } catch (Exception e) {
//            System.out.println("----1----");
            this.close();
        }
    }

    public boolean isTrue(){
        return this.name != null;
    }

    /**
     * 接收方法
     *
     * @return 返回该客户接收到的消息
     */
    private String receive() {
        try {
            return dis.readUTF();
        } catch (Exception e) {
//            System.out.println("----2----");
            this.close();
        }
        return "";
    }

    /**
     * 得到客户端用户名
     *
     * @return 客户端用户名
     */
    public String getName() {
        return this.name;
    }

    /**
     * 发送消息
     *
     * @param msg 消息
     */
    private void send(String msg) {
        try {
            if (!msg.equals("")) {
                dos.writeUTF(msg);
                dos.flush();
            }
        } catch (Exception e) {
//            System.out.println("----3----");
            this.close();
        }
    }


    /**
     * 群聊与私聊
     * 私聊协议：@XXX ....
     * 先判断是否是遵守规定私聊协议
     *
     * @param msg   需要发送的消息
     * @param isOff 是否掉线
     */
    private void sendAllClient(String msg, boolean isOff) {
        //先判断是否是私聊
        boolean isPrivate = msg.startsWith("@");
        int i = 0;
        if (isPrivate) {// 私聊
            int index = msg.indexOf(" ");
            String clientName = msg.substring(1, index);// 截取到用户名
            if (clientName.equals(this.name)) return;
            msg = msg.substring(index + 1);              // 截取到消息
            if (!msg.equals("")) {
                for (ClientContent client : server.getClientList()) {
                    if (client.name.equals(clientName)) {
                        client.send(this.name + "@了你：" + msg);// 发送给指定用户
                        i++;
                    }
                }
                if(i==0) send("该用户不在线上！");
            }
        } else {// 群聊
            for (ClientContent client : server.getClientList()) {
                if (client == this) {
                    continue;
                }
                if (!isOff && !msg.equals("")) {
                    String date = LYLTool.showData();
                    client.send(this.name + ":" + date + "\r\n\t" + msg);
                } else {
                    client.send(msg);
                }
            }
        }
    }

    /**
     * 关闭
     */
    public void close() {
        isRunning = false;
        server.getClientList().remove(this);
        LYLTool.closeAllIO(dos, dis, socket);
        // 发送给所有人该客户端退出了群聊
        sendAllClient(this.name + "离开了聊天室", true);
    }
}
