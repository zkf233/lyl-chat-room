# LYL Chat Room

#### 介绍
{**简易聊天室（后端）**
采用局域网后端控制台通信，主要用SocketServer，Socket来写服务端和客户端。
}

#### 软件架构
- 主要是ClientContent类：
    每一个Client连接到服务器，就会产生一个ClientContent的实例，将Client的Socket传递过来，然后在Server的ClientList中存储便于Server操作容器中的客户端
- Server：
    ClientList容器，用于存储Client产生的实例，因为不能让Client直接操作Server中的ClientList，只能将Client的Socket传递给ClientContent来实现实例化客户端的目的，保证了Client是操作不了ClientList，保护Client的稳定性；

#### 使用说明
1. Client用于产生客户端
2. Server初始化服务器
3. util包下是发送端和接收端，每个Client都要开启的
4. ClientContent用于接收Client的Socket产生实例，便于Server管理ClientList


